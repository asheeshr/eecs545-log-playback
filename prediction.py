#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from imports import *

def generate_patches(image, stride=256, window_size=(256, 256)):
    patches = []
    positions = []
    
    idx = 0
    for x in range(0, image.shape[0], stride):
        idx = idx + 1
        idy = 0
        for y in range(0, image.shape[1], stride):
            idy = idy + 1
            new_patch = image[x:x + window_size[0], y:y + window_size[1]]
            if new_patch.shape[:2] == window_size:
                patches.append(new_patch)
                positions.append((idx, idy))
    return patches, positions

def stitch_patches(patches, positions):
    image_stitched = np.zeros((patches[0].shape[0]*max(positions)[0], 
                               patches[0].shape[1]*max(positions)[1],
                               3), np.uint8)
    idn = 0    
    for idx, idy in positions:
        x = (idx - 1)*patches[idn].shape[0]
        y = (idy - 1)*patches[idn].shape[1]
        image_stitched[
                    x:x + patches[idn].shape[0], y:y + patches[idn].shape[1]
                    ] = patches[idn]
        idn = idn + 1
        
    return image_stitched 

    
def load_net(model, weights):
    caffe.set_mode_gpu();
    caffe.set_device(0);
#    global net
    net = caffe.Net(model, weights, caffe.TEST)
    return net

def normalize_image(image):
    image_new = image.copy()
    image_new[:, :, 0] = cv2.equalizeHist(image_new[:, :, 0])
    image_new[:, :, 1] = cv2.equalizeHist(image_new[:, :, 1])
    image_new[:, :, 2] = cv2.equalizeHist(image_new[:, :, 2])
#    image_new = image.astype('float')
#    print image.mean(axis=(0,1))
#    print image_new.shape
#    return image_new - image_new.mean(axis=(0,1))
    return image_new


def init_transformer(net, mean_pixel):
    """ Defines the Caffe transformer that will be able to preprocress the data
        before feeding it to the neural network
    """
    # Initialize the transformer
    transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    # Normalize the data by substracting the mean pixel
#    MEAN_PIXEL = [81.29, 81.93, 81.00] ## WHAT IS THE SOLUTION FOR THIS?
    transformer.set_mean('data', np.asarray(mean_pixel))
    # Reshape the data from numpy to Caffe format (channels first : WxHxC -> CxWxH)
    transformer.set_transpose('data', (2,0,1))
    # Data is expected to be in [0, 255] (int 8bit encoding)
    transformer.set_raw_scale('data', 255.0)
    # Transform from RGB to BGR
    #    if BGR:
    transformer.set_channel_swap('data', (2,1,0))
    
    return transformer, net

def label_to_pixel(label):
    codes = [[0, 0, 0], #Clutter
             [255, 255, 0], # Roads
             [0, 0, 255] # Buildings
             ]
    return np.asarray(codes[int(label)])

def process_patches(images, net, transformer, model_family):
    # caffe.io.load_image converts to [0,1], so our transformer sets it back to [0,255]
    # but the skimage lib already works with [0, 255] so we convert it to float with img_as_float
    data = np.zeros(net.blobs['data'].data.shape)
    for i in range(len(images)):
        data[i] = transformer.preprocess('data', img_as_float(images[i]))
    net.forward(data=data)
    if 'segnet' in model_family:
        output = net.blobs['conv1_1_D'].data[:len(images)]
    if 'alexnet' in model_family:
        output = net.blobs['score'].data[:len(images)]
        
    output = np.swapaxes(np.swapaxes(output, 1, 3), 1, 2)
    return output

def process_votes(prediction):
    rgb = np.zeros(prediction.shape[:2] + (3,), dtype='uint8')
    for x in xrange(prediction.shape[0]):
        for y in xrange(prediction.shape[1]):
            rgb[x,y] = np.asarray(label_to_pixel(np.argmax(prediction[x,y])))
    return rgb


def predict_patches(patches, net, transformer, model_family):
    predictions = []
    for p in process_patches(patches, net, transformer, model_family):
        predictions.append(p)
#        print p.dtype
#        cv2.imshow('pred1', p)
    rgb_predictions = [process_votes(pred) for pred in predictions]
    return rgb_predictions


def post_process_patches_pred(patches_pred):
#    patches_pred_resized = [
#            cv2.resize(img, (256,256), interpolation=cv2.INTER_NEAREST) 
#            for img in patches_pred
#            ]
    patches_pred = [cv2.medianBlur(patch, 7) for patch in patches_pred]
    return patches_pred

def post_process_img_pred(img_pred):
#    print img_pred.shape
#    img_pred_resized = cv2.resize(img_pred, (256*4, 256*3), interpolation=cv2.INTER_NEAREST)
    img_pred_resized = cv2.resize(img_pred, (256*3, 256*3), interpolation=cv2.INTER_NEAREST)
#    print img_pred_resized.shape
    return cv2.cvtColor(img_pred_resized, cv2.COLOR_BGR2RGB)
