#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from imports import *

def generate_edges(img):
    img2 = img.copy()
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
#    blur = cv2.GaussianBlur(img_gray,(5,5),0)
    ret3, img_edges = cv2.threshold(img_gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

#    img_edges = cv2.Canny(img,150,250)
    im2, contours, hierarchy = cv2.findContours(img_edges, 
                                                cv2.RETR_TREE, 
                                                cv2.CHAIN_APPROX_SIMPLE)
    print np.asarray(contours).shape
#    cnt = contours[4]
    blank_image = np.zeros(img.shape)
    cv2.drawContours(blank_image, contours, -1, (0,255,0), 1)

#    img_ret = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
#    print img_edges.shape
    return blank_image, contours
#def generate_statistics():
#    stats = np.zeros

def combine_contour_output(image_contour, image_output, contours):
    # loop over the contours
    image_contour = image_contour.astype('uint8')
    mask = np.ones(image_contour.shape[:2], np.uint8)

    mask = cv2.bitwise_and(np.array(image_contour[:,:,2] == 255, np.uint8), mask)
#    mask = mask.astype('uint8')
    print image_contour.shape
    mask = cv2.resize(mask, 
                      (image_contour.shape[0]+2, image_contour.shape[1]+2)
                      )
    print mask.shape
    print mask.dtype
    print image_contour.dtype
#    print mask.shape

    final = np.zeros(image_contour.shape,np.uint8)
    gray = cv2.cvtColor(image_output,cv2.COLOR_RGB2GRAY)
    mask2 = np.zeros(gray.shape,np.uint8)
    
    for i in xrange(0,len(contours)):
        mask2[...]=0
        cv2.drawContours(mask2,contours,i,255,-1)
#        print "med", np.median(image_output[mask2 == 255], axis=0)
        cv2.drawContours(final,contours,i,np.median(image_output[mask2 == 255], axis=0),-1)

    for c in contours:
    	# compute the center of the contour
    	M = cv2.moments(c)
        if M["m00"] == 0:
            continue
    	cx = int(M["m10"] / M["m00"])
    	cy = int(M["m01"] / M["m00"])
        col = np.array((int(final[cx, cy][0]), 
                       int(final[cx, cy][1]),
                       int(final[cx, cy][2])))
#        print col.dtype

        cv2.floodFill(image_contour, 
                      None,
                      (cx, cy),
                      col)
    return image_contour
    

def segment_watershed(img, markers2):
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#    ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    ret, markers = cv2.connectedComponents(np.uint8(gray))
#    markers = markers+1
#    markers[unknown==255] = 0
#    
    markers = cv2.watershed(img,markers)
    img[markers == -1] = [0,0,0]
    return img

