#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from imports import *
from segment import *
from prediction import *
from application import *
from log_playback import log_playback

def nothing(arg):
    pass
    
def cleanup():
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    for i in range(1,10):
        cv2.waitKey(1)
    return

def main():
    
    model = "";
    weights = "";
    log = "";

    argv = sys.argv
    model, weights, log = get_arguments(argv)

    if not os.path.isfile(model):
        print 'model file ' + model + ' doesnt exist'
        return
    
    if not os.path.isfile(weights):
        print 'weights file ' + weights + ' doesnt exist'
        return
        
    if not os.path.isfile(log):
        print "log file " + log + ' doesnt exist'
        return

    if 'segnet' in model:
        model_family = 'segnet'
    if 'alexnet' in model:
        model_family = 'alexnet'
    
    
    window_title = 'Semantic Segmentation'
    cv2.startWindowThread()
    cv2.namedWindow(window_title)
    
    logfile = log_playback(log)
    
#    logfile = 
    cv2.createTrackbar('Time', window_title, 
                       0, logfile.get_length(), nothing)
    cv2.createTrackbar('Skip Frames', window_title, 0, 10, nothing)
    cv2.setTrackbarPos('Skip Frames', window_title, 0)
    
    cv2.createTrackbar('Playback', window_title, 0, 1, nothing)
    cv2.setTrackbarPos('Playback', window_title, 1)
    
    last_position = 0
    curr_position = 0
    time_loop = 0
    save_image = False
    save_image_name = ''
    skip_frames = 0
    skipped = 0
    
    patch_size = (256*3//2, 256*3//2)
    stride = 256*3//2
    
    if model_family is 'segnet':
        mean_pixel = [82, 82, 82] # Segnet 
#        mean_pixel = [129.8, 129.8, 129.8] # Segnet norm <--- SWITCH THIS!!
        
    if model_family is 'alexnet':
        mean_pixel = [85, 92, 85] # Alexnet 
    
    net = load_net(model, weights)
    transformer, net = init_transformer(net, mean_pixel)
    
    while 1:        
        time_loop_start = time.time()
        
        curr_position = cv2.getTrackbarPos('Time', window_title)
        if curr_position != last_position:
            logfile.set_position(curr_position)
        img = logfile.get_frame()
        last_position = logfile.get_position()
        cv2.setTrackbarPos('Time', window_title, last_position)

        skip_frames = cv2.getTrackbarPos('Skip Frames', window_title)
        if img is not None:
            if skipped < skip_frames:
                skipped = skipped + 1
                continue
            
            skipped = 0
#            img = read_image_from_event(event)
            img_norm = normalize_image(img)
#            img_hist = generate_color_hist(img_norm)
#            img_edges, contours = generate_edges(img)

            img_patches, img_patches_positions = generate_patches(img_norm, 
                                                                  stride, 
                                                                  patch_size)
            time_prediction_start = time.time()
            img_patches_pred = predict_patches(img_patches, 
                                               net, 
                                               transformer, 
                                               model_family)
            
            img_patches_pred = post_process_patches_pred(img_patches_pred)
            time_prediction = time.time() - time_prediction_start            

            patch_blend = True
            if patch_blend:
                img_patches_blend = [
                        blend_transparent(patch, pred) 
                        for patch, pred in zip(img_patches, img_patches_pred)
                ]
                img_output_blend = stitch_patches(img_patches_blend, 
                                                  img_patches_positions)
                img_output_blend_post = post_process_img_pred(img_output_blend)
                
            img_output = stitch_patches(img_patches_pred, 
                                         img_patches_positions)
            img_output_post = post_process_img_pred(img_output)

            if not patch_blend:
                img_output_post_blend = blend_transparent(img, img_output_post)
            
#            img_watershed = segment_watershed(img, img_output_post)
#            img_output_contour = combine_contour_output(img_edges, img_output_post, contours)
            img_text = generate_debugging(img, 
                                          [
                                                  'DEBUGGING',
                                                  '',
                                                  'Model Family: ' + model_family,
                                                  'Model: ' + os.path.basename(model),
                                                  'Weights: ' + os.path.basename(weights),
                                                  'Log File: ' + os.path.basename(log),
                                                  '',
                                                  'Loop Time: ' + str(int(time_loop*1000)) + 'ms',
                                                  'Pred Time: ' + str(int(time_prediction*1000)) + 'ms',
                                                  '',
                                                  'Patches: ' + str(len(img_patches)),
                                                  'Patch Size: ' + str(patch_size),
                                                  '',
                                                  'Saved Image: ' + save_image_name \
                                                  if save_image else '',
                                          ])
            img_list = [img_norm, 
                        img_output_post, 
                        img_output_post_blend if not patch_blend else img_output_blend_post, 
#                        img_hist,
#                        img_edges,
#                        img_output_contour
#                        None,
                        img_text
                        ]
            img_names = ['Input', 'Prediction', 'Blended', None]
            
            img_display = generate_display(img_list, img_names)
            cv2.imshow(window_title, img_display)
            
            save_image = False
            k = cv2.waitKey(50) & 0xFF
            if k == 32:
                save_image_name = '/var/tmp/' + os.path.basename(log) + str(curr_position) + '.png'
                cv2.imwrite(save_image_name, 
                            img_output_post_blend if not patch_blend 
                            else img_output_blend_post)
                save_image = True
            
        time_loop = time.time() - time_loop_start
        while not cv2.getTrackbarPos('Playback', window_title) or img is None:
            k = cv2.waitKey(100) & 0xFF
            if k == 27:
                cleanup()
                return
    cleanup()
    return

if __name__ == "__main__":
    main()