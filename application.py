#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from imports import *

def get_arguments(argv):

    model = ''
    weights = ''
    log = ''

    help_message = __file__ + \
        '--model <model-file> ' + \
        '--weights <weights-file> ' + \
        '--log <log> '

    try:
        opts, args = getopt.getopt(argv[1:],
                                   "hm:w:l:",
                                   ["model=",
                                    "weights=", 
                                    "log="])
    except getopt.GetoptError:
        print help_message
        return

    for opt, arg in opts:
        if opt == '-h':
            print help_message 
            sys.exit()
        elif opt in ("m", "--model"):
            model = arg
        elif opt in ("w", "--weights"):
            weights = arg
        elif opt in ("l", "--log"):
            log = arg
        
    return model, weights, log
    
def generate_display(images, names, shape=(2,2)):
    image_display = np.zeros((images[0].shape[0]*shape[0], 
                             images[0].shape[1]*shape[1], 
                             3), np.uint8)
    idx = 0
    for x in range(0, image_display.shape[0], images[0].shape[0]):
        for y in range(0, image_display.shape[1], images[0].shape[1]):
            if images[idx] is not None:
                if names[idx] is not None:
                    cv2.putText(images[idx], 
                                names[idx],
                                (30, 60), 
                                cv2.FONT_HERSHEY_SIMPLEX, 
                                1, 
                                (255,255,255)
                                )
#                print inspect.stack()[0][3], names[idx], images[idx].shape
                image_display[
                        x:x + images[idx].shape[0],
                        y:y + images[idx].shape[1],
                        :
                        ] = cv2.copyMakeBorder(images[idx][5:images[idx].shape[0]-5, 
                                               5:images[idx].shape[1]-5],
                    5,5,5,5,
                    cv2.BORDER_CONSTANT,
                    value=[255,255,255])    
            idx = idx + 1
            if idx >= len(images):
                return image_display
    return image_display

def blend_non_transparent(image, overlay):
    gray_overlay = cv2.cvtColor(overlay, cv2.COLOR_BGR2GRAY)
    overlay_mask = cv2.threshold(gray_overlay, 1, 255, cv2.THRESH_BINARY)[1]

    overlay_mask = cv2.erode(overlay_mask, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    overlay_mask = cv2.blur(overlay_mask, (3, 3))

    background_mask = 255 - overlay_mask

    overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
    background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)

    image_part = (image * (1 / 255.0)) * (background_mask * (1 / 255.0))
    overlay_part = (overlay * (1 / 255.0)) * (overlay_mask * (1 / 255.0))

    return np.uint8(cv2.addWeighted(image_part, 255.0, overlay_part, 255.0, 0.0))

def blend_transparent(image, overlay_t_img):
    
    if image.shape != overlay_t_img.shape:
        overlay_t_img = cv2.resize(overlay_t_img, 
                                   (image.shape[1], image.shape[0]), 
                                   interpolation = cv2.INTER_NEAREST)

    overlay_img = overlay_t_img[:,:,:3] # Grab the BRG planes
    overlay_mask = np.ones(overlay_img.shape[0:2], dtype=overlay_img.dtype)*175

    black_pixels = np.asarray(np.logical_and( 
            np.logical_and(overlay_t_img[:,:,0] == 0,
                           overlay_t_img[:,:,1] == 0),
                           overlay_t_img[:,:,2] == 0))

    overlay_mask[black_pixels] = 0    
    background_mask = 255 - overlay_mask

    overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
    background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)

    image_part = (image * (1 / 255.0)) * (background_mask * (1 / 255.0))
    overlay_part = (overlay_img * (1 / 255.0)) * (overlay_mask * (1 / 255.0))
    
    return np.uint8(cv2.addWeighted(image_part, 255.0, overlay_part, 255.0, 0.0))


def generate_debugging(img, text_list):
    img_text = np.zeros(img.shape, np.uint8)
    y0, dy = 60, 40

    for idx, line in enumerate(text_list):    
        y = y0 + idx*dy

        cv2.putText(img_text, 
                    line,
                    (30, y), 
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    1, 
                    (255,255,255)
                    )
    
    return img_text

def generate_color_hist(img):
    h = np.zeros((300,256,3))
 
    bins = np.arange(256).reshape(256,1)
    color = [ (255,0,0),(0,255,0),(0,0,255) ]
     
    for ch, col in enumerate(color):
        hist_item = cv2.calcHist([img],[ch],None,[256],[0,255])
        cv2.normalize(hist_item,hist_item,0,255,cv2.NORM_MINMAX)
        hist=np.int32(np.around(hist_item))
        pts = np.column_stack((bins,hist))
        cv2.polylines(h,[pts],False,col)
     
    h=np.flipud(h)
    h = cv2.resize(h, (img.shape[1], img.shape[0]), interpolation=cv2.INTER_NEAREST)
#    print img.shape
#    print h.shape
    return h
