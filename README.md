This application can be used to run CNN models on logs. 

This depends on Caffe, Caffe-SegNet, and OpenCV 3. 
It is compatible with Python 2.

This requires an network structure defined in a prototxt file, network model 
defined in a caffemodel file, and a log file that can be LCM or MP4. The network 
outputs should be limited to 3 classes.

Run `sem_seg_log.py --help` to see argument format.

![Screenshot of the UI](ui.png)

