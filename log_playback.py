#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from imports import *


class log_playback:
    def __init__(self, name, image_size = (256*3, 256*3)):
        self.file_type = None
        self.file_type = 'lcm' if 'lcm' in name else self.file_type
        self.file_type = 'mp4' if 'mp4' in name else self.file_type
        
        print self.file_type
        assert self.file_type in ['lcm', 'mp4']
        
        if self.file_type is 'lcm':
            self.file = lcm.EventLog(name, "r")
            self.file_length = self.file.size()//1e3
        if self.file_type is 'mp4':
            self.file = imageio.get_reader(name,  'ffmpeg')
            self.file_length = self.file.get_length()
        self.frame_size = image_size
        
        self.pos = 0
    
    def get_length(self):
        return int(self.file_length)
    
    def get_file_type(self):
        return self.file_type
    
    def get_frame_size(self):
        return self.frame_size
    
    def get_frame(self):
        '''
        Returns the frame from any input stream. Does not fix size.
        Returns None when no more frames.
        '''
        frame = None
        if self.file_type is 'lcm':
            frame = self.__read_frame_from_lcm()
        
        if self.file_type is 'mp4':
            frame = self.__read_frame_from_mp4()
            self.pos += 1
        return frame
    
    def get_position(self):
        if self.file_type is 'lcm':
            self.pos = self.file.tell()//1e3
        
        if self.file_type is 'mp4':
            pass
        return int(self.pos)
    
    def set_position(self, new_pos):
        if self.file_type is 'lcm':
            self.pos = self.file.seek(new_pos*1e3)
        
        if self.file_type is 'mp4':
            self.pos = new_pos
        return self.pos
    
    def __read_frame_from_lcm(self):
        '''
        Reads frame from LCM file if present.
        '''
        event = self.file.read_next_event()
        while event is not None and "IMAGE_COLOR" not in event.channel:
            event = self.file.read_next_event()
        if event is None:
            return None
        return self.__read_image_from_event(event)

    def __read_image_from_event(self, event):
        '''
        Returns img with BGR. Crops image to fixed size.
        Should be fixed at some point.
        '''
        msg = image_t.decode(event.data)
        by = array.array('B', msg.data)
        na = np.array(by, np.uint8)
        na = na[::-1]
        na = na.reshape((msg.height, msg.width))
        img = cv2.cvtColor(na, cv2.COLOR_BAYER_GB2BGR)
    #    img = cv2.resize(img, image_size) # ?? RESIZE or CROP??
        img = img[img.shape[0]/2 - 256*3/2:img.shape[0]/2 - 256*3/2 + 256*3,
                  img.shape[1]/2 - 256*3/2:img.shape[1]/2 - 256*3/2 + 256*3
                  ]
        ## Cropping looks better. Pespective doesnt get warped.
    #    img = img[:,256/2:256/2+image_size[1]]        
        return img
    
    def __read_frame_from_mp4(self):
        '''
        Returns frame from mp4 in BGR. Crops image to fixed size.
        Should be fixed at some point.
        '''
        image = self.file.get_data(self.pos)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
#        print image.shape
#        image = image[image.shape[0]/2 - 128*2/2 : image.shape[0]/2 + 128*2/2,
#                      image.shape[1]/2 - 128*3/2 : image.shape[1]/2 + 128*3/2]
        image = cv2.resize(image, (256*3, 256*4)) # ?? RESIZE or CROP??
        image = image[
                image.shape[0]/2 - 256*3/2:image.shape[0]/2 - 256*3/2 + 256*3,
                image.shape[1]/2 - 256*3/2:image.shape[1]/2 - 256*3/2 + 256*3
                ]

        return image
    
