#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
os.environ['GLOG_minloglevel'] = '2' 
#import caffe
import getopt
import lcm
import sys
import numpy as np
#import matplotlib.pyplot as plt
#from PIL import Image                                                                                
from image_t import image_t
import array
#import io
import cv2
import inspect
import time
#import timeit 
from skimage import img_as_float, io

import imageio


## NEEDED FOR SEGNET
CAFFE_ROOT = '/home/asheeshr/april/DeepNetsForEO/caffe/'
sys.path.insert(0, CAFFE_ROOT + 'python/')
import caffe
